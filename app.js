var { CONFIG } = require("./config");
var express = require("express");
var app = express();

var rutas = require('./src/routes/index');

const port = process.env.PORT || CONFIG.PORT;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', rutas);
app.get('/', (req, res)=> {
    res.status(200).send("Aluvir System");
});

app.listen (port, ()=> {
    console.log('El servidor esta corriendo en el puerto: ' + port)
})