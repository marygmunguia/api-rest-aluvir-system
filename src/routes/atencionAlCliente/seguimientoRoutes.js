const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const {
    registrarSeguimiento,
    actualizarSeguimiento,
    eliminarSeguimiento,
    consultarRegistrosSeguimientos,
    consultarRegistroSeguimiento,
} = require("../../controllers/atencionAlCliente/seguimientoController");

app.post("/registrarSeguimiento", validarToken, registrarSeguimiento);
app.post("/eliminarSeguimiento", validarToken, eliminarSeguimiento);
app.put("/actualizarSeguimiento", validarToken, actualizarSeguimiento);
app.get(
    "/consultarRegistrosSeguimientos",
    validarToken,
    consultarRegistrosSeguimientos
);
app.get(
    "/consultarRegistroSeguimiento",
    validarToken,
    consultarRegistroSeguimiento
);

module.exports = app;
