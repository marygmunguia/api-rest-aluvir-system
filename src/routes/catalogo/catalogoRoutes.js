const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { guardarProducto, consultarProductoAgregado, consultarTodosLosProductos, consultarProductoExistente } = require("../../controllers/catalogo/catalogoControllers");

app.post('/guardarProducto', validarToken, guardarProducto);
app.get('/consultarProductoAgregado', validarToken, consultarProductoAgregado);
app.get('/consultarTodosLosProductos', validarToken, consultarTodosLosProductos);
app.post('/consultarProductoExistente', validarToken, consultarProductoExistente);

module.exports = app;

