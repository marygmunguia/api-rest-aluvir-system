const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const {
  agregarVentas,
  eliminarVentas,
  actualizarVentas,
  consultarVentas,
  consultarVenta,
  productosMasVendidos,
  consultarVentaPorFecha, fechaMasVentas
} = require("../../controllers/ventas/ventasController");

app.post("/agregarVentas", validarToken, agregarVentas);
app.put("/actualizarVentas", validarToken, actualizarVentas);
app.delete("/eliminarVentas", validarToken, eliminarVentas);
app.get("/consultarVentas", consultarVentas);
app.get("/consultarVenta", consultarVenta);
app.get("/consultarVentaPorFecha", consultarVentaPorFecha);

// Reportes
app.get("/productosMasVendidos", productosMasVendidos);
app.get("/fechaMasVentas", fechaMasVentas);

module.exports = app;
