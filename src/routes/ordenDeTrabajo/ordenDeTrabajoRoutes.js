const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarOrdenDeTrabajo } = require("../../controllers/ordenDeTrabajo/ordenDeTrabajoController");

app.post('/agregarOrdenDeTrabajo', validarToken, agregarOrdenDeTrabajo);

module.exports = app;