const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarProveedor, actualizarProveedor, eliminarProveedor, consultarProveedor, consultarProveedores } = require("../../controllers/proveedores/proveedoresControllers");

app.post('/agregarProveedor', validarToken, agregarProveedor);
app.put('/actualizarProveedor', validarToken, actualizarProveedor);
app.delete('/eliminarProveedor', validarToken, eliminarProveedor);
app.post('/consultarProveedor', validarToken, consultarProveedor);
app.get('/consultarProveedores', validarToken, consultarProveedores);
app.get('/consultarProveedoresReporte', consultarProveedores);

module.exports = app;
