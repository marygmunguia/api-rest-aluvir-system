const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const {
  obtenerUsuarios,
  agregarUsuario,
  desactivarUsuario,
  cambiarImagenUsuario,
  ingresoApi,
  consultarUltimoUsuario,
  cambiarClave,
  recuperarAcceso,
} = require("../../controllers/usuarios/usuariosController");

app.get("/obtenerUsuarios", validarToken, obtenerUsuarios);
app.get("/obtenerUsuariosReporte", obtenerUsuarios);
app.get("/consultarUltimoUsuario", validarToken, consultarUltimoUsuario);
app.post("/agregarUsuario", validarToken, agregarUsuario);
app.post("/cambiarImagenUsuario", validarToken, cambiarImagenUsuario);
app.post("/desactivarUsuario", validarToken, desactivarUsuario);
app.post("/cambiarClave", validarToken, cambiarClave);
app.get("/recuperarAcceso", recuperarAcceso);

app.post("/ingreso", ingresoApi);

module.exports = app;
