const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarCategoria, actualizarCategoria, eliminarCategoria, consultarCategorias, consultarCategoria } = require("../../controllers/categorias/categoriasControllers");

app.post('/agregarCategoria', validarToken, agregarCategoria);
app.put('/actualizarCategoria', validarToken, actualizarCategoria);
app.delete('/eliminarCategoria', validarToken, eliminarCategoria);
app.get('/consultarCategorias', validarToken, consultarCategorias);
app.post('/consultarCategoria', validarToken, consultarCategoria);

module.exports = app;