const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarAcceso, consultarAcceso } = require("../../controllers/sistema/sistemasController");
const { consultarPermiso, otorgarPermiso } = require("../../controllers/sistema/permisosController");
const { consultarRoles, consultarAcciones, consultarRolAcceso, ultimoRolAgregado, agregarAccesoAlRol,
agregarNuevoRol, ultimoRolAccesoAgregado , agregarAccionAlAccesoDelRol, eliminarNuevoRol } = require('../../controllers/sistema/rolesController');

app.post('/agregarAcceso', validarToken, agregarAcceso);
app.post('/consultarPermiso', validarToken, consultarPermiso);
app.get('/consultarAcceso', validarToken, consultarAcceso);
app.post("/otorgarPermiso", validarToken, otorgarPermiso);

app.get('/consultarRoles', validarToken, consultarRoles);
app.get('/consultarAcciones', validarToken, consultarAcciones);
app.post('/consultarRolAcceso', validarToken, consultarRolAcceso);
app.post('/agregarNuevoRol', validarToken, agregarNuevoRol);
app.get('/ultimoRolAgregado', validarToken ,ultimoRolAgregado);
app.post('/agregarAccesoAlRol', validarToken, agregarAccesoAlRol);
app.get('/ultimoRolAccesoAgregado', validarToken, ultimoRolAccesoAgregado);
app.post('/agregarAccionAlAccesoDelRol', validarToken, agregarAccionAlAccesoDelRol);
app.delete("/eliminarNuevoRol", validarToken, eliminarNuevoRol);

module.exports = app;