const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarBitacora, consultarBitacoraPorFecha, consultarBitacoraPorIdUsuario, consultarTodaBitacora } = require("../../controllers/seguridad/bitacorasController");

app.post('/agregarBitacora', validarToken, agregarBitacora);
app.get('/consultarTodaBitacora', validarToken, consultarTodaBitacora);
app.get('/consultarBitacoraPorIdUsuario', validarToken, consultarBitacoraPorIdUsuario);
app.get('/consultarBitacoraPorFecha', validarToken, consultarBitacoraPorFecha);

module.exports = app;