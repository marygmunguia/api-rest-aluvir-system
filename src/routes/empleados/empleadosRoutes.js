const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const {
  agregarEmpleado,
  actualizarEmpleado,
  eliminarEmpleado,
  consultarEmpleado,
  consultarUltimoEmpleado,
  consultarEmpleados,
} = require("../../controllers/usuarios/empleadosController");

app.post("/agregarEmpleado", validarToken, agregarEmpleado);
app.put("/actualizarEmpleado", validarToken, actualizarEmpleado);
app.delete("/eliminarEmpleado", validarToken, eliminarEmpleado);
app.get("/consultarEmpleado", validarToken, consultarEmpleado);
app.get("/consultarUltimoEmpleado", validarToken, consultarUltimoEmpleado);
app.get("/consultarEmpleados", validarToken, consultarEmpleados);

module.exports = app;
