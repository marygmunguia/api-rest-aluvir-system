const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarCuentas, actualizarCuentas, eliminarCuentas, } = require("../../controllers/cuentas/cuentasController");

app.post("/agregarCuentas", validarToken, agregarCuentas);
app.put("/actualizarCuentas", validarToken, actualizarCuentas);
app.delete("/eliminarCuentas", validarToken, eliminarCuentas);

module.exports = app;
