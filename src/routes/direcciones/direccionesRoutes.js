const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarNuevaDireccion, actualizarDireccion, eliminarDireccion, consultarDirecciones } = require("../../controllers/direcciones/direccionesControllers");

app.post('/agregarNuevaDireccion', validarToken, agregarNuevaDireccion);
app.put('/actualizarDireccion', validarToken, actualizarDireccion);
app.delete('/eliminarDireccion', validarToken, eliminarDireccion);
app.get('/consultarDirecciones', validarToken, consultarDirecciones);

module.exports = app;