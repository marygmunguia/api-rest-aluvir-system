const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarMetodopago, actualizarMetodopago, eliminarMetodopago, consultarMetodospago } = require("../../controllers/metodospago/metodospagoControllers");

app.post('/agregarMetodopago', validarToken, agregarMetodopago);
app.put('/actualizarMetodopago', validarToken, actualizarMetodopago);
app.delete('/eliminarMetodopago', validarToken, eliminarMetodopago);
app.get('/consultarMetodospago', validarToken, consultarMetodospago);

module.exports = app;