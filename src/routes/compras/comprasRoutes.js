const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarCompra, actualizarCompra, eliminarCompra, consultarCompra, consultarCompras } = require("../../controllers/compras/comprasControllers");

app.post('/agregarCompra', validarToken, agregarCompra);
app.put('/actualizarCompra', validarToken, actualizarCompra);
app.delete('/eliminarCompra', validarToken, eliminarCompra);
app.get('/consultarCompra', validarToken, consultarCompra);
app.get('/consultarCompras', validarToken, consultarCompras);
module.exports = app;