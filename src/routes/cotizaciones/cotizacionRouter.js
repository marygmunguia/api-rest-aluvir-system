const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const {
  agregarCotizacion,
  actualizarCotizacion,
  eliminarCotizacion,
  consultarCotizacion,
  consultarUltimaCotizacion,
  agregarDetallesCotizacion,
  consultarCotizaciones,
  consultarDetallesCotizacion,
  consultarCotizacionVenta,
  consultarCotizacionesVenta
} = require("../../controllers/cotizaciones/cotizacionControllers");

app.post("/agregarCotizacion", validarToken, agregarCotizacion);
app.put("/actualizarCotizacion", validarToken, actualizarCotizacion);
app.delete("/eliminarCotizacion", validarToken, eliminarCotizacion);
app.get("/consultarCotizacion", validarToken, consultarCotizacion);
app.get('/consultarUltimaCotizacion', validarToken, consultarUltimaCotizacion);
app.post('/agregarDetallesCotizacion', validarToken, agregarDetallesCotizacion);
app.get('/consultarCotizaciones',validarToken, consultarCotizaciones);
app.post('/consultarDetallesCotizacion', validarToken, consultarDetallesCotizacion);
app.post('/consultarDetallesCotizacionImprimir', consultarDetallesCotizacion);
app.get('/consultarCotizacionImprimir', consultarCotizacion);
app.get('/consultarCotizacionVenta', consultarCotizacionVenta);
app.get('/consultarCotizacionesVenta', consultarCotizacionesVenta);

module.exports = app;
