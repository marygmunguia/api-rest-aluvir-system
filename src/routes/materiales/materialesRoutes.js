const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const {
  agregarMaterial,
  actualizarMaterial,
  eliminarMaterial,
  consultarMateriales,
  registrarIngresoMaterial,
  materialPorAgotarse,
} = require("../../controllers/materiales/materialesControllers");

app.post("/agregarMaterial", validarToken, agregarMaterial);
app.put("/actualizarMaterial", validarToken, actualizarMaterial);
app.delete("/eliminarMaterial", validarToken, eliminarMaterial);
app.get("/consultarMateriales", validarToken, consultarMateriales);
app.post("/registrarIngresoMaterial", validarToken, registrarIngresoMaterial);

app.get("/materialPorAgotarse", materialPorAgotarse);

module.exports = app;
