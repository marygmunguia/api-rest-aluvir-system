const express = require("express");
const app = express.Router();

const { validarToken } = require("../../middlewares/token");
const { agregarClientes, eliminarClientes, actualizarClientes, consultarClientes, 
	consultarTiposIdentificaciones, consultarTipoIdentificacion } = require("../../controllers/clientes/clientesController");

app.post("/agregarClientes", validarToken, agregarClientes);
app.delete("/eliminarClientes", validarToken, eliminarClientes);
app.put("/actualizarClientes", validarToken, actualizarClientes);
app.get("/consultarClientes", validarToken, consultarClientes);
app.get("/consultarTiposIdentificaciones", validarToken, consultarTiposIdentificaciones);
app.get("/consultarTipoIdentificacion", validarToken, consultarTipoIdentificacion);
app.get("/consultarClientesReporte", consultarClientes);

module.exports = app;
