const mysql = require('mysql');
const { CONFIG } = require('../../config');

class dbHelper {
    constructor(){

    }

    #retornarConexion(){
        return mysql.createConnection({
            host: CONFIG.HOST,
            user: CONFIG.USER,
            password: CONFIG.PASSWORD,
            database: CONFIG.DATABASE,
            port: CONFIG.DB_PORT
        });
    }
    
    query(consulta, paramentos = []){
        return new Promise((resolve, reject) => {
            try {
                const conexion = this.#retornarConexion();

                conexion.connect(error => {
                    if(error){
                        reject(`No se pudo conectar a la base de datos ${error}`);
                    }
                });

                conexion.query(consulta, paramentos, (error, results, fields) => {
                    if(error){
                        reject(`No se pudo realizar la consulta ${error}`);
                    }
                    resolve(results);
                });
                conexion.end();

            } catch ({ message }) {
                reject(`Ocurrio un error interno ${message}`);
            }
        });
    } 


}

module.exports = dbHelper;