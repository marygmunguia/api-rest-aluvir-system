const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarCuentas = async (req, res) => {
  const { body } = req;
  const { codigoCliente, saldoCliente } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_AGREGAR_CUENTAS(?,?)",
      [codigoCliente, saldoCliente]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha agregado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

const actualizarCuentas = async (req, res) => {
  const { body } = req;
  const { codigo, codigoCliente, saldoCliente } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ACTUALIZAR_CUENTAS_CLIENTES(?,?,?)",
      [codigo, codigoCliente, saldoCliente]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha actualizado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

const eliminarCuentas = async (req, res) => {
  const { body } = req;
  const { codigo } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_CUENTAS(?)", [
      codigo,
    ]);
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha eliminado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

module.exports = { agregarCuentas, actualizarCuentas, eliminarCuentas };
