const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

var consultarRoles = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_ROLES()");
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

var consultarAcciones = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_ACCIONES()");
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

var consultarRolAcceso = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    const { body } = req;
    const { codigoRol } = body; 

    try {
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_ROL_ACCESO(?)", [codigoRol]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


var agregarNuevoRol = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    const { body } = req;
    const { nombre, usuario } = body; 

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_ROLES(?,?)", [nombre, usuario]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

var agregarAccesoAlRol = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    const { body } = req;
    const { rol, acceso } = body; 

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_ACCESO_AL_ROL(?,?)", [rol, acceso]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


var ultimoRolAgregado = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_ULTIMO_ROL_AGREGADO()");
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


var ultimoRolAccesoAgregado = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_ACCESO_ACCION_AGREGADO()");
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

var agregarAccionAlAccesoDelRol = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    const { body } = req;
    const { codigo, accion } = body; 

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_ROL_PERMISO_ACCION(?,?)", [codigo, accion]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


var eliminarNuevoRol = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    const { body } = req;
    const { nombre, usuario } = body; 

    try {
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_ROL(?,?)", [nombre, usuario]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

module.exports = { consultarRoles, consultarAcciones, consultarRolAcceso, agregarNuevoRol, 
    agregarAccesoAlRol, ultimoRolAgregado, ultimoRolAccesoAgregado, 
    agregarAccionAlAccesoDelRol, eliminarNuevoRol }