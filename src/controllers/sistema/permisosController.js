const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

var consultarPermiso = async (req, res) => {

	const { body } = req;
    const { usuario, acceso } = body;

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try{
    	const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_PERMISO(?,?)", [usuario, acceso]);
    	resHttp.respuesta = datos;

      return res.status(200).send(resHttp);

    }catch(error){
    	resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }

}

var otorgarPermiso = async (req, res) => {

    const { body } = req;
    const { usuario, acceso } = body;

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try{
        const datos = await databasehelper.query("CALL AS_SP_OTORGAR_PERMISO(?,?)", [usuario, acceso]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    }catch(error){
        resHttp.existeError = true;
        resHttp.mensaje = error.mensaje;

        return res.status(400).send(resHttp);
    }

}

module.exports = { consultarPermiso, otorgarPermiso };
