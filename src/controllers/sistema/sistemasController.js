const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

var agregarAcceso = async (req,res) =>{
    
    const { body } = req;
    const { nombre, detalle } = body;
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_ACCESO_SISTEMA(?,?)", [nombre, detalle]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha agregado el registro correctamente";

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


var consultarAcceso = async (req,res) =>{
    
    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_ACCESOS_SISTEMA()");
        resHttp.respuesta = datos[0];

        return res.status(200).send(resHttp);

    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


module.exports = { agregarAcceso, consultarAcceso }