const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const guardarProducto = async (req, res) => {

  const { body } = req;
  const { nombre, detalle, imagen, precio } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_AGREGAR_PRODUCTO(?,?,?,?)", [nombre, detalle, imagen, precio]);
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha agregado el registro correctamente";

    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

const consultarProductoAgregado = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_PRODUCTO_AGREGADO()");
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};


const consultarTodosLosProductos = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_PRODUCTOS_EXISTENTES()");
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};


const consultarProductoExistente = async (req, res) => {

  const { body } = req;
  const { codigo } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_PRODUCTO_EXISTENTE(?)", [codigo]);
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

module.exports = { guardarProducto, consultarProductoAgregado, consultarTodosLosProductos, consultarProductoExistente };