const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

var agregarEmpleado = async (req, res) => {
  const { body } = req;
  const {
    nombres,
    apellidos,
    fechaNac,
    sexo,
    direccion,
    telefono,
    celular,
    email,
  } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_AGREGAR_EMPLEADO(?,?,?,?,?,?,?,?)",
      [nombres, apellidos, fechaNac, sexo, direccion, telefono, celular, email]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Empleado agregado correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = Error;

    return res.status(400).send(resHttp);
  }
};

var actualizarEmpleado = async (req, res) => {
  const { body } = req;
  const {
    codigo,
    nombres,
    apellidos,
    fechaNac,
    sexo,
    direccion,
    telefono,
    celular,
    email,
  } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ACTUALIZAR_EMPLEADO(?,?,?,?,?,?,?,?,?)",
      [
        codigo,
        nombres,
        apellidos,
        fechaNac,
        sexo,
        direccion,
        telefono,
        celular,
        email,
      ]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "El Empleado a sido Actualizado Correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var eliminarEmpleado = async (req, res) => {
  const { body } = req;
  const { codigo } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ELIMINAR_EMPLEADO(?)",
      [codigo]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Empleado Eliminado Exitosamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = Error;

    return res.status(400).send(resHttp);
  }
};

var consultarEmpleado = async (req, res) => {
  const { codigo } = req.query;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_CONSULTAR_EMPLEADO(?)",
      [codigo]
    );

    resHttp.respuesta = datos;
    resHttp.mensaje = "Empleado Encontrado";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var consultarEmpleados = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_INFO_USUARIOS()");

    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);
    
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var consultarUltimoEmpleado = async (req, res) => {
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_ULTIMO_EMPLEADO()");

    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

module.exports = {
  agregarEmpleado,
  actualizarEmpleado,
  eliminarEmpleado,
  consultarEmpleado,
  consultarUltimoEmpleado,
  consultarEmpleados
};
