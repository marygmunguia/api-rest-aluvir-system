const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");
const {
  encryptarContrasena,
  compararContrasena,
} = require("../../tools/encryption");
const { generarToken } = require("../../tools/jwt");

var obtenerUsuarios = async (req, res) => {
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_OBTENER_USUARIOS()");
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var agregarUsuario = async (req, res) => {
  const { body } = req;
  const { nombre, pass, img, estado, codEmpleado, Admin } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  const passEncryptada = await encryptarContrasena(pass);

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_AGREGAR_USUARIO(?,?,?,?,?,?)",
      [nombre, passEncryptada, img, estado, codEmpleado, Admin]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha agregado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var desactivarUsuario = async (req, res) => {
  const { body } = req;
  const { CODIGO } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const respuesta = await databasehelper.query(
      "CALL AS_SP_DESACTIVAR_USUARIO(?)",
      [CODIGO]
    );
    resHttp.respuesta = respuesta;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

const cambiarImagenUsuario = async (req, res) => {
  const { body } = req;
  const { RUTA_IMAGEN, CODIGO } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const respuesta = await databasehelper.query(
      "CALL AS_SP_CAMBIAR_IMAGEN_USUARIO(?,?)",
      [RUTA_IMAGEN, CODIGO]
    );
    resHttp.respuesta = respuesta;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

const ingresoApi = async (req, res) => {
  const { body } = req;
  const { nombre, clave } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const usuario = await databasehelper.query(
      "CALL AS_SP_INGRESO_USUARIO(?)",
      [nombre]
    );

    if (usuario.length === 0) {
      resHttp.existeError = true;
      resHttp.mensaje = "Usuario Incorrecto";
      return res.status(400).send(resHttp);
    }

    const contrasenaCorrecta = await compararContrasena(
      clave,
      usuario[0][0].PASS_USUARIO
    );

    if (!contrasenaCorrecta) {
      resHttp.existeError = true;
      resHttp.mensaje = "Contraseña Incorrecta";
      return res.status(400).send(resHttp);
    }

    const datosToken = {
      id: usuario[0][0].PK_ID_USUARIO,
      usuario: usuario[0][0].NOM_USUARIO,
    };

    const token = generarToken(datosToken);

    resHttp.respuesta = { token, usuario };
    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;
    return res.status(400).send(resHttp);
  }
};

var consultarUltimoUsuario = async (req, res) => {
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_ULTIMO_USUARIO()");

    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};


var cambiarClave = async (req, res) => {

  const { body } = req;
  const { codigo, nombre, clave, claveNueva } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {

    const passEncryptada = await encryptarContrasena(claveNueva);

    const usuario = await databasehelper.query(
      "CALL AS_SP_INGRESO_USUARIO(?)",
      [nombre]
    );

    if (usuario.length === 0) {
      resHttp.existeError = true;
      resHttp.mensaje = "Usuario Incorrecto";
      return res.status(400).send(resHttp);
    }

    const contrasenaCorrecta = await compararContrasena(
      clave,
      usuario[0][0].PASS_USUARIO
    );

    if (!contrasenaCorrecta) {
      resHttp.existeError = true;
      resHttp.mensaje = "Contraseña Incorrecta";
      return res.status(400).send(resHttp);
    }

    const datos = await databasehelper.query("CALL AS_SP_NUEVA_CLAVE(?,?)", [passEncryptada, codigo]);

    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);


  }catch (error) {

    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }

}


var recuperarAcceso = async (req, res) => {

  const { CORREO, CLAVE } = req.query;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {

    const passEncryptada = await encryptarContrasena(CLAVE);

    const usuario = await databasehelper.query(
      "CALL AS_SP_CONSULTAR_USUARIO_RP(?)",
      [CORREO]
    );

    const datos = await databasehelper.query("CALL AS_SP_NUEVA_CLAVE(?,?)", [passEncryptada, usuario[0][0].PK_ID_USUARIO]);
    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);


  }catch (error) {

    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }

}

module.exports = {
  obtenerUsuarios,
  agregarUsuario,
  desactivarUsuario,
  cambiarImagenUsuario,
  ingresoApi,
  consultarUltimoUsuario,
  cambiarClave,
  recuperarAcceso,
};
