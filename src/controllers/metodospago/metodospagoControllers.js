const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarMetodopago = async (req,res) => {
    const { body } = req;
    const { NOMBRE, DETALLE } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_METODOP(?,?)", [NOMBRE, DETALLE]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha registrado la accion en Metodos de Pago";
         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }
}

const actualizarMetodopago = async (req,res) => {
    const { body } = req;
    const { COD, NOMBRE, DETALLE } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {

        const datos = await databasehelper.query("CALL AS_SP_ACTUALIZAR_METODOP(?,?,?)", [COD, NOMBRE, DETALLE]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha actualizado la accion en Metodos de Pago";
         
        return res.status(200).send(resHttp);

    } catch (error) {
        
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const eliminarMetodopago = async (req,res) => {
    const { body } = req;
    const { COD } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_METODOP(?)", [COD]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha eliminado la informacion de Metodos de Pago";
         
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarMetodospago = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_METODOP()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


module.exports = { agregarMetodopago, actualizarMetodopago, eliminarMetodopago, consultarMetodospago }