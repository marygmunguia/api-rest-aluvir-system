const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarClientes = async (req, res) => {
  const { body } = req;
  const {
    tipoIdentificacion,
    numeroIdentificacion,
    nombre,
    apellido,
    email,
    telefono,
    celular,
    credito,
    limiteCredito,
  } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_AGREGAR_CLIENTES(?,?,?,?,?,?,?,?,?)",
      [
        tipoIdentificacion,
        numeroIdentificacion,
        nombre,
        apellido,
        email,
        telefono,
        celular,
        credito,
        limiteCredito,
      ]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha agregado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

const eliminarClientes = async (req, res) => {
  const { body } = req;
  const { codigo } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ELIMINAR_CLIENTES(?)",
      [codigo]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha eliminado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(200).send(resHttp);
  }
};

const actualizarClientes = async (req, res) => {
  const { body } = req;
  const {
    codigo,
    tipoIdentificacion,
    numeroIdentificacion,
    nombre,
    apellido,
    email,
    telefono,
    celular,
    credito,
    limiteCredito,
  } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ACTUALIZAR_CLIENTES(?,?,?,?,?,?,?,?,?,?)",
      [
        tipoIdentificacion,
        numeroIdentificacion,
        nombre,
        apellido,
        email,
        telefono,
        celular,
        credito,
        limiteCredito,
        codigo
      ]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha actualizado el registro correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(200).send(resHttp);
  }
};

const consultarClientes = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_CLIENTES()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


const consultarTiposIdentificaciones = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_TIPOS_IDENTIFICACIONES()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarTipoIdentificacion = async (req,res) => {

    const { codigo } = req.query;

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_TIPO_IDENTIFICACION(?)", [codigo]);
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

module.exports = { agregarClientes, eliminarClientes, actualizarClientes, 
  consultarClientes, consultarTiposIdentificaciones, consultarTipoIdentificacion };
