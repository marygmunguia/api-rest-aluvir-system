const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarNuevaDireccion = async (req,res) => {
    const { body } = req;
    const { CODCLIENTE, DEPARTAMENTO, MUNICIPIO, CIUDAD, COLONIA, DETALLE } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_DIRECCIONES(?,?,?,?,?,?)", [CODCLIENTE, DEPARTAMENTO, MUNICIPIO, CIUDAD, COLONIA, DETALLE]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha registrado la accion en direcciones";
         
        return res.status(200).send(resHttp);
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    } catch (error) {
        
    }
}

const actualizarDireccion = async (req,res) => {
    const { body } = req;
    const { COD, CODCLIENTE, DEPARTAMENTO, MUNICIPIO, CIUDAD, COLONIA, DETALLE } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_ACTUALIZAR_DIRECCIONES_CLIENTES(?,?,?,?,?,?,?)", [ COD, CODCLIENTE, DEPARTAMENTO, MUNICIPIO, CIUDAD, COLONIA, DETALLE]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha actualizado la accion en direcciones";
         
        return res.status(200).send(resHttp);
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    } catch (error) {
        
    }

}

const eliminarDireccion = async (req,res) => {
    const { body } = req;
    const { COD } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_DIRECCIONES(?)", [COD]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha eliminado la accion en direcciones";
         
        return res.status(200).send(resHttp);
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    } catch (error) {
        
    }

}

const consultarDirecciones = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_DIRECCIONES()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

module.exports = { agregarNuevaDireccion, actualizarDireccion, eliminarDireccion, consultarDirecciones }
