const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarOrdenDeTrabajo = async (req,res) => {
    const { body } = req;
    const { EMPLEADO, COTIZACION, FECHA, DETALLE,ESTADO } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_ORDEN_TRABAJO(?,?,?,?,?)", [EMPLEADO, COTIZACION, FECHA, DETALLE,ESTADO]);
        resHttp.respuesta = datos;
         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }
}

module.exports = { agregarOrdenDeTrabajo }