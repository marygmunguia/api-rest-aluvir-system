const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarBitacora = async (req,res) => {

    const { body } = req;
    const { usuario, accion, detalle } = body;

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_REGISTRO_BITACORA(?,?,?)", [usuario, accion, detalle]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha registro la accion en la bitacora";

        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }

}

const consultarTodaBitacora = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_TODA_BITACORA()");
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }

}

const consultarBitacoraPorIdUsuario = async (req,res) => {

    const request = req.query;

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_REGISTRO_BITACORA_POR_USUARIO(?)", 
        [request.codigo]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }

}

const consultarBitacoraPorFecha = async (req,res) => {

    const request = req.query;

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_REGISTRO_BITACORA(?,?)", 
        [request.finicio, request.ffinal]);
        resHttp.respuesta = datos;

        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }

}


module.exports = { agregarBitacora, consultarBitacoraPorFecha, 
    consultarBitacoraPorIdUsuario, consultarTodaBitacora }