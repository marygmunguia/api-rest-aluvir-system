const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarMaterial = async (req,res) => {
    const { body } = req;
    const { CODIGO, NOMBRE, DETALLE, CODCATEGORIA, PRECIO, EXISTENCIA, MINIMO, MAXIMO } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_INVENTARIO(?,?,?,?,?,?,?,?)", [CODIGO, NOMBRE, DETALLE, CODCATEGORIA, PRECIO, EXISTENCIA, MINIMO, MAXIMO]);
        resHttp.respuesta = datos;
         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }
}

const actualizarMaterial = async (req,res) => {
    const { body } = req;
    const { COD, CODIGO, NOMBRE, DETALLE, CODCATEGORIA, PRECIO, MINIMO, MAXIMO } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {

        const datos = await databasehelper.query("CALL AS_SP_ACTUALIZAR_INVENTARIO(?,?,?,?,?,?,?,?)", [COD, CODIGO, NOMBRE, DETALLE, CODCATEGORIA, PRECIO, MINIMO, MAXIMO]);
        resHttp.respuesta = datos;
         
        return res.status(200).send(resHttp);

    } catch (error) {
        
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const eliminarMaterial = async (req,res) => {
    const { body } = req;
    const { COD } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_INVENTARIO(?)", [COD]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha eliminado la accion en Materiales";
         
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarMateriales = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_INVENTARIO()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const materialPorAgotarse = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_PRODUCTO_POR_AGOTARSE()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


const registrarIngresoMaterial = async (req,res) => {
    
    const { body } = req;
    const { CODIGO, EXISTENCIA, USUARIO } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_REGISTRAR_INGRESO_MATERIAL(?,?,?)", [ CODIGO, EXISTENCIA, USUARIO ]);
        resHttp.respuesta = datos;
         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }
}


module.exports = { agregarMaterial, actualizarMaterial, eliminarMaterial, 
    consultarMateriales, registrarIngresoMaterial, materialPorAgotarse }