const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

var agregarCotizacion = async (req, res) => {
  const { body } = req;
  const { cliente, usuario, subtotal, descuento, isv, total } =
    body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_AGREGAR_NUEVA_COTIZACION(?,?,?,?,?,?)",
      [cliente, usuario, subtotal, descuento, isv, total]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha agregado la cotización correctamente";

    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var actualizarCotizacion = async (req, res) => {
  const { body } = req;
  const { codigo, identidad, fecha, hora, subtotal, descuento, isv, total } =
    body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ACTUALIZAR_COTIZACION(?,?,?,?,?,?,?,?)",
      [codigo, identidad, fecha, hora, subtotal, descuento, isv, total]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha Actualizado su Cotizacion correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = Error;

    return res.status(400).send(resHttp);
  }
};

var eliminarCotizacion = async (req, res) => {
  const { body } = req;
  const { codigo } = body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ELIMINAR_COTIZACION(?)",
      [codigo]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha Eliminado su Cotizacion Correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = Error;

    return res.status(400).send(resHttp);
  }
};

var consultarCotizacion = async (req, res) => {
  const { codigo } = req.query;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_COTIZACION(?)", [
      codigo,
    ]);
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha Encontrado su cotizacion con Exito";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};

var consultarCotizacionVenta = async (req, res) => {
  const { codigo } = req.query;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_COTIZACION_VENTA(?)", [
      codigo,
    ]);
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha Encontrado su cotizacion con Exito";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};

var consultarCotizacionesVenta = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_COTIZACIONES_VENTA()");
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha Encontrado su cotizacion con Exito";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};


var consultarDetallesCotizacion = async (req, res) => {
  const { codigo } = req.body;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_DETALLES_COTIZACION(?)", [
      codigo,
    ]);
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};


var consultarUltimaCotizacion = async (req, res) => {
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {

    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_ULTIMA_COTIZACION()");
    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);

  } catch (error) {

    resHttp.existeError = true;
    resHttp.mensaje = error;
    return res.status(400).send(resHttp);

  }
};


var agregarDetallesCotizacion = async (req, res) => {

  const { body } = req;
  const { cotizacion, producto, cantidad, precio } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {

    const datos = await databasehelper.query("CALL AS_SP_AGREGAR_DETALLE_COTIZACION(?,?,?,?)",
      [cotizacion, producto, cantidad, precio]);
    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);

  } catch (error) {

    resHttp.existeError = true;
    resHttp.mensaje = error;
    return res.status(400).send(resHttp);

  }
};

var consultarCotizaciones = async (req, res) => {
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {

    const datos = await databasehelper.query("CALL AS_SP_COTIZACIONES_EXISTENTES()");
    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);

  } catch (error) {

    resHttp.existeError = true;
    resHttp.mensaje = error;
    return res.status(400).send(resHttp);

  }
};

module.exports = {
  agregarCotizacion,
  actualizarCotizacion,
  eliminarCotizacion,
  consultarCotizacion,
  consultarUltimaCotizacion,
  agregarDetallesCotizacion,
  consultarCotizaciones,
  consultarDetallesCotizacion,
  consultarCotizacionVenta,
  consultarCotizacionesVenta,
};
