const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarCategoria = async (req,res) => {
    const { body } = req;
    const { NOMBRE, DETALLE } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_CATEGORIA(?,?)", [NOMBRE, DETALLE]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha registrado la accion en Categoria";
         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }
}

const actualizarCategoria = async (req,res) => {
    const { body } = req;
    const { COD, NOMBRE, DETALLE } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {

        const datos = await databasehelper.query("CALL AS_SP_ACTUALIZAR_CATEGORIA(?,?,?)", [COD, NOMBRE, DETALLE]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha actualizado la accion en Categoria";
         
        return res.status(200).send(resHttp);

    } catch (error) {
        
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const eliminarCategoria = async (req,res) => {
    const { body } = req;
    const { COD } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_CATEGORIA(?)", [COD]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha eliminado la Categoria";
         
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarCategorias = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_CATEGORIAS()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarCategoria = async (req,res) => {

    const { body } = req;
    const { COD } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_CATEGORIA(?)", [COD]);
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

module.exports = { agregarCategoria, actualizarCategoria, eliminarCategoria, consultarCategorias, consultarCategoria }