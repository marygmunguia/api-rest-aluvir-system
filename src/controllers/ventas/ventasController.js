const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarVentas = async (req, res) => {
  const { body } = req;
  const {
    NO_FACTURA,
    COD_COTIZACION,
    COD_ORDEN_TRABAJO,
    ID_USUARIO_VENTA,
    TIPO_PAGO,
    COD_METODO_PAGO,
    SUBTOTAL,
    DESCUENTO,
    ISV,
    TOTAL_PAGAR
  } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_AGREGAR_VENTA(?,?,?,?,?,?,?,?,?,?)",
      [
        NO_FACTURA,
        COD_COTIZACION,
        COD_ORDEN_TRABAJO,
        ID_USUARIO_VENTA,
        TIPO_PAGO,
        COD_METODO_PAGO,
        SUBTOTAL,
        DESCUENTO,
        ISV,
        TOTAL_PAGAR
      ]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Se ha agregado la venta";
    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;
    return res.status(400).send(resHttp);
  }
};

const actualizarVentas = async (req, res) => {
  const { body } = req;
  const {
    COD_VENTA,
    NO_FACTURA,
    COD_COTIZACION,
    COD_ORDEN_TRABAJO,
    ID_USUARIO_VENTA,
    FEC_VENTA,
    HORA_VENTA,
    TIPO_PAGO,
    COD_METODO_PAGO,
    SUBTOTAL,
    DESCUENTO,
    ISV,
    TOTAL_PAGAR
  } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query(
      "CALL AS_SP_ACTUALIZAR_VENTA(?,?,?,?,?,?,?,?,?,?,?)",
      [
        COD_VENTA,
        NO_FACTURA,
        COD_COTIZACION,
        COD_ORDEN_TRABAJO,
        ID_USUARIO_VENTA,
        TIPO_PAGO,
        COD_METODO_PAGO,
        SUBTOTAL,
        DESCUENTO,
        ISV,
        TOTAL_PAGAR,
      ]
    );
    resHttp.respuesta = datos;
    resHttp.mensaje = "Su venta se actualizo correctamente";

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = Error;

    return res.status(400).send(resHttp);
  }
};

var eliminarVentas = async (req, res) => {
  const { body } = req;
  const { COD_VENTA } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_VENTA(?)", [COD_VENTA]);
    resHttp.respuesta = datos;
    resHttp.mensaje = "La venta se ha eliminado correctamente";

    return res.status(200).send(resHttp);
  
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.messaje;

    return res.status(400).send(resHttp);
  }
};

var consultarVentas = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_VENTAS()");
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};


var consultarVenta = async (req, res) => {

  const { codigo } = req.query;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_VENTA(?)", [codigo]);
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var consultarVentaPorFecha = async (req, res) => {

  const { INICIO, FINAL } = req.query;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_VENTAS_FECHAS(?,?)", [INICIO, FINAL]);
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

// REPORTES DE VENTAS

var productosMasVendidos = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_MAS_VENDIDOS()");
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

var fechaMasVentas = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_MAS_VENTAS()");
    resHttp.respuesta = datos;

    return res.status(200).send(resHttp);
  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error;

    return res.status(400).send(resHttp);
  }
};

module.exports = { agregarVentas, eliminarVentas, actualizarVentas, consultarVentas, consultarVenta, 
  productosMasVendidos, consultarVentaPorFecha,  fechaMasVentas};
