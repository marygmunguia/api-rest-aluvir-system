const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarProveedor = async (req,res) => {
    const { body } = req;
    const { rtn, nombre, direccion, correo, telefono, fax, celular, sitioweb, asesor, asesorcorreo, asesorcel } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_PROVEEDOR(?,?,?,?,?,?,?,?,?,?,?)", [rtn, nombre, direccion, correo, telefono, fax, celular, sitioweb, asesor, asesorcorreo, asesorcel]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha registrado el registro del Proveedor";
         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }
}

const actualizarProveedor = async (req,res) => {
    const { body } = req;
    const { cod, rtn, nombre, direccion, correo, telefono, fax, celular, sitioweb, asesor, asesorcorreo, asesorcel } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {

        const datos = await databasehelper.query("CALL AS_SP_ACTUALIZAR_PROVEEDOR(?,?,?,?,?,?,?,?,?,?,?,?)", [cod, rtn, nombre, direccion, correo, telefono, fax, celular, sitioweb, asesor, asesorcorreo, asesorcel]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha actualizado el registro del Proveedor";
         
        return res.status(200).send(resHttp);

    } catch (error) {
        
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const eliminarProveedor = async (req,res) => {
    const { body } = req;
    const { cod } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_PROVEEDOR(?)", [cod]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha eliminado el registro del Proveedor";
         
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarProveedor = async (req,res) => {
    const { cod } = req.body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_PROVEEDOR(?)", [cod]);
        resHttp.respuesta = datos;
         
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const consultarProveedores = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_PROVEEDORES()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


module.exports = { agregarProveedor, actualizarProveedor, eliminarProveedor, consultarProveedor, consultarProveedores }

