const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const agregarCompra = async (req,res) => {
    const { body } = req;
    const { FACTURA, CODPROVEEDOR, FECHA, SUBTOTAL, DESCUENTO, ISV, TOTAL } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_AGREGAR_COMPRA(?,?,?,?,?,?,?)", [FACTURA, CODPROVEEDOR, FECHA, SUBTOTAL, DESCUENTO, ISV, TOTAL]);
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);
    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

const actualizarCompra = async (req,res) => {
    const { body } = req;
    const { COD, FACTURA, CODPROVEEDOR, FECHA, SUBTOTAL, DESCUENTO, ISV, TOTAL } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        
        const datos = await databasehelper.query("CALL AS_SP_ACTUALIZAR_COMPRA(?,?,?,?,?,?,?,?)", [COD, FACTURA, CODPROVEEDOR, FECHA, SUBTOTAL, DESCUENTO, ISV, TOTAL]);
        resHttp.respuesta = datos;         
        return res.status(200).send(resHttp);

    } catch (error) {

        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
        
    }

}

const eliminarCompra = async (req,res) => {
    const { body } = req;
    const { COD } = body; 

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
        const datos = await databasehelper.query("CALL AS_SP_ELIMINAR_COMPRA(?)", [COD]);
        resHttp.respuesta = datos;
        resHttp.mensaje = "Se ha eliminado la accion en Compras";
         
        return res.status(200).send(resHttp);
    } catch (error) {
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }

}

const consultarCompra = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_COMPRA()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}


const consultarCompras = async (req,res) => {

    const databasehelper = new dbHelper();
    const resHttp = new responseHttp();

    try {
      
        const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_COMPRAS()");
        resHttp.respuesta = datos;
        return res.status(200).send(resHttp);

    } catch (error) {
       
        resHttp.existeError = true;
        resHttp.mensaje = error;

        return res.status(400).send(resHttp);
    }
}

module.exports = { agregarCompra, actualizarCompra, eliminarCompra, consultarCompra, consultarCompras }
