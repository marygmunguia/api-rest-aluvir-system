const dbHelper = require("../../database/dbHelper");
const responseHttp = require("../../tools/response");

const registrarSeguimiento = async (req, res) => {
  const { body } = req;
  const { VENTA, CLIENTE, DETALLE, CALIFICACION } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const respuesta = await databasehelper.query("CALL AS_SP_REGISTRAR_SEGUIMIENTO(?,?,?,?)", [
      VENTA,
      CLIENTE,
      DETALLE,
      CALIFICACION
    ]);

    resHttp.respuesta = respuesta;
    resHttp.mensaje = "El registro del seguimiento se ha agregado correctamente";
    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};

const actualizarSeguimiento = async (req, res) => {
  const { body } = req;
  const { CODIGO, VENTA, CLIENTE, DETALLE, CALIFICACION } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const respuesta = await databasehelper.query("CALL AS_SP_ACTUALIZAR_SEGUIMIENTO(?,?,?,?,?)", [
      CODIGO,
      VENTA,
      CLIENTE,
      DETALLE,
      CALIFICACION
    ]);

    resHttp.respuesta = respuesta;
    resHttp.mensaje = "El registro del seguimiento se ha actualizado correctamente";
    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};

const eliminarSeguimiento = async (req, res) => {
  const { body } = req;
  const { CODIGO } = body;

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const respuesta = await databasehelper.query("CALL AS_SP_ELIMINAR_SEGUIMIENTO(?)", [CODIGO]);

    resHttp.respuesta = respuesta;
    resHttp.mensaje = "El registro del seguimiento se ha eliminado correctamente";
    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};

const consultarRegistrosSeguimientos = async (req, res) => {

  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_SEGUIMIENTOS()");

    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};


const consultarRegistroSeguimiento = async (req, res) => {

  const { codigo } = req.query;
  const databasehelper = new dbHelper();
  const resHttp = new responseHttp();

  try {
    const datos = await databasehelper.query("CALL AS_SP_CONSULTAR_SEGUIMIENTO(?)", [codigo]);

    resHttp.respuesta = datos;
    return res.status(200).send(resHttp);

  } catch (error) {
    resHttp.existeError = true;
    resHttp.mensaje = error.message;

    return res.status(400).send(resHttp);
  }
};

module.exports = { registrarSeguimiento, actualizarSeguimiento , eliminarSeguimiento, consultarRegistrosSeguimientos, consultarRegistroSeguimiento };
